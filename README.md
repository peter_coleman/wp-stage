# WP-Stage
A simple light weight tool that deploys a site based on a config file.

## Install
`yarn` or `npm install`

### Usage
This project is not yet finished, you'll need to manually run the script.

`node src`

Keep in mind this script will look for a file called `wp-stage.config.json` in the current working directory. It will also build the site in a dir called `.up` which will be created in the current working directory.

### config file
This tool looks for a config file in the working directory called `wp-stage.config.json` it should have the following shape.

```
git: {
   remote: // wp git remote to push to
   gitignore: [...//files to ignore]
},
plugins: [
   ...can have as many as you want
   {
       name: // name of plugin
       git: {
           repo: // the git repo to use
           branch: // branch to use
       },
       build: [...// build steps to run]
   }
],
themes:  [
   ...can have as many as you want
   {
       name: // name of theme
       git: {
           repo: // the git repo to use
           branch: // branch to use
       },
       build: [...// build steps to run]
   }
]
```

### Road map
As mentioned above this project is not yet finished below I've listed it's current state and the remaining tasks.

**Current State**

* Consumes & validates config
* Creates site structure
* clones and builds themes and plugins

**Remaining tasks**

* Finish writing the deployment through git
* refactor and clean
* Test and write tests
* Make it easier to use
* deploy as an npm package
