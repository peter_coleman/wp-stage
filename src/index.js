const program = require('commander');
const { get } = require('object-path');
const { promisify } = require('util');
const fs = require('fs');
const { error } = require('./utils/log');

// Actions
const validate = require('./actions/validate');
const deploy = require('./actions/deploy');

// Handles
const handleDir = require('./handles/handle-dir');
const handlePluginsOrThemes = require('./handles/handle-plugins-themes');
const handleGit = require('./handles/handle-git');

// Promisify
const readFileAsync = promisify(fs.readFile);

// setup commander
program
  .version('0.0.1')
  .option(
    '-c --config [path]',
    'Specify a path for the up.config.json file other than the default'
  )
  .option('-d --directory [path]', 'Directory to build site in')
  .option('-f --force', 'Force a complete rebuild')
  .parse(process.argv);

// handle main function
main(program)
  .then(data => {
    console.log('Succesfully deployed: ' + data.url);
    process.exit(1);
  })
  .catch(err => {
    error('Error: ' + err);
    process.exit(0);
  });

/**
 * Async fucntion that handles the control flow of actions when deploying
 * @param  {object} program object from commander, containers args
 * @return {Promise}
 */
async function main (program) {
  const cwd = process.cwd();
  const configFileName = 'up.config.json';

  // set the configPath either by default or arg
  const configPath = get(program, 'config') || cwd + '/' + configFileName;

  // Check if up.config.json exists
  if (!fs.existsSync(configPath)) {
    error(
      `Error: ${configFileName} not found in working directory ${cwd}
       create one here or see --help to specify an alternative location`
    );
    process.exit(0);
  }

  // load the config
  const config = await readFileAsync(configPath)
    .then(data => JSON.parse(data))
    .catch(error);

  // try and catch an errors in config here before we do anything else
  const errors = validate(config);
  if (errors.length) {
    error(
      'Error: config did not pass validation for the following reasons',
      errors
    );
  }

  const dir = handleDir(get(program, 'directory'), cwd);

  // Setup plugins
  if (get(config, 'plugins.length')) {
    await handlePluginsOrThemes(dir + '/wp-content/plugins/', [
      ...get(config, 'plugins')
    ]);
  }

  // Setup themes
  if (get(config, 'themes.length')) {
    await handlePluginsOrThemes(dir + '/wp-content/themes/', [
      ...get(config, 'themes')
    ]);
  }

  // process.exit(1); // completed to here

  // setup git
  await handleGit({ ...get(config, 'git') });

  // deploy
  await deploy();
}
