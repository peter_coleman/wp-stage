const { validate: jsonValidate } = require('jsonschema');
const { get } = require('object-path');

module.exports = function validate (config) {
  const errors = [];
  errors.push(...get(jsonValidate(config, schema), 'errors'));

  // check required
  if (!get(config, 'git.remote')) {
    errors.push('config.git.remote is required');
  }

  if (
    !get(config, 'plugins') &&
    !get(config, 'plugins').every(
      plugin => get(plugin, 'name') && get(plugin, 'repo')
    )
  ) {
    errors.push('All plugins require the fields name and repo');
  }

  if (
    !get(config, 'themes') &&
    !get(config, 'themes').every(
      theme => get(theme, 'name') && get(theme, 'repo')
    )
  ) {
    errors.push('All themes require the fields name and repo');
  }

  return errors;
};

const schema = {
  id: 'configSchema',
  properties: {
    git: {
      id: '/properties/git',
      properties: {
        gitignore: {
          items: {
            type: 'string'
          },
          type: 'array'
        },
        remote: {
          type: 'string'
        }
      },
      type: 'object'
    },
    plugins: {
      items: {
        properties: {
          build: {
            items: {
              type: 'string'
            },
            type: 'array'
          },
          git: {
            properties: {
              branch: {
                type: 'string'
              },
              repo: {
                type: 'string'
              }
            },
            type: 'object'
          },
          name: {
            type: 'string'
          }
        },
        type: 'object'
      },
      type: 'array'
    },
    themes: {
      items: {
        properties: {
          build: {
            items: {
              type: 'string'
            },
            type: 'array'
          },
          git: {
            properties: {
              branch: {
                type: 'string'
              },
              repo: {
                type: 'string'
              }
            },
            type: 'object'
          },
          name: {
            type: 'string'
          }
        },
        type: 'object'
      },
      type: 'array'
    }
  },
  type: 'object'
};
