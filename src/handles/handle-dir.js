const fs = require('fs');

/**
 * Return the directory path in which to build the site, create it if it does not exist
 * @param  {string} directory directory in which to create site
 * @param  {string} cwd       current working directory
 * @return {string}           directory to build site in
 */
module.exports = function handleDir (directory, cwd) {
  const dirName = '.up';
  const dirPath = directory || cwd + '/' + dirName;

  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
  }

  return dirPath;
};
