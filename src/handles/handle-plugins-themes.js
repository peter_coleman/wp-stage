const fs = require('fs');
const simpleGit = require('simple-git/promise');
const Promise = require('bluebird');
const shelljs = require('shelljs');
const mkdirp = require('mkdir-promise');
const { get } = require('object-path');

/**
 * handles preparing plugins for deployment
 * @param  {string}  dir     The directory we are building the site in
 * @param  {Array}   plugins plugins to be prepared
 * @param  {boolean} force   Force total refresh
 * @return {Promise}
 */
module.exports = async function handlePluginsOrThemes (
  pluginsDir,
  plugins,
  force
) {
  // if we don't have any plugins bail here
  if (!plugins.length) return Promise.resolve();

  // ensure we have a plugins directory
  if (!fs.existsSync(pluginsDir)) {
    await mkdirp(pluginsDir);
  }

  return Promise.each(plugins, async function (plugin) {
    // ensure we have the plugin directory
    const pluginDir = pluginsDir + plugin.name;
    if (!fs.existsSync(pluginDir)) {
      fs.mkdirSync(pluginDir);
    }

    // is this a git directory
    const git = simpleGit(pluginDir).silent(true);
    const isGitDir = fs.existsSync(pluginDir + '/.git');
    let shouldBuild = true;

    // either pull or clone
    if (isGitDir) {
      const pullSummary = await git.pull().then();
      shouldBuild = !!get(pullSummary, 'files.length');
    } else {
      const options = get(plugin, 'git.branch')
        ? [`-b ${get(plugin, 'git.branch')}`]
        : [];
      await git.clone(get(plugin, 'git.repo'), pluginDir, options);
    }

    // if there were any changes re-build or build for first time
    if (shouldBuild) {
      shelljs.cd(pluginDir);
      const failedBuilds = (plugin.build || []).filter(
        build => !shelljs.exec(build)
      );
      if (failedBuilds.length) {
        throw Error('the following builds failed' + failedBuilds.join(', '));
      }
    }
  });
};
