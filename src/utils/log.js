const chalk = require('chalk');
const log = console.log;

/**
 * log out a message to the console using the error colour (red)
 * @param  {string} error error message
 * @param  [string] hex   hex colour to override
 * @return {undefined}
 */
module.exports.error = function (error, hex) {
  log((hex ? chalk(hex) : chalk.red)(error));
};

/**
 * log out a message to the console using the message colour (green)
 * @param  {string} message message
 * @param  [string] hex     hex colour to override
 * @return {undefined}
 */
module.exports.message = function (message, hex) {
  log((hex ? chalk(hex) : chalk.green)(message));
};
